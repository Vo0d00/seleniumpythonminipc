from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


from util.random import RandomUtil
project_names = []


class AddProjectPage:

    URL = 'http://demo.testarena.pl/administration/add_project'

    def __init__(self, browser):
        self.browser = browser



    def load(self):
        self.browser.get(self.URL)

    def fill_form_with_random_data(self):
        name = RandomUtil.get_random_string(10)
        prefix = RandomUtil.get_random_string(5)
        description = RandomUtil.get_random_string(20)

        self.browser.find_element(By.CSS_SELECTOR, 'input[name="name"]').send_keys(name)
        self.browser.find_element(By.CSS_SELECTOR, 'input[name="prefix"]').send_keys(prefix)
        self.browser.find_element(By.CSS_SELECTOR, 'textarea[name="description"]').send_keys(description)

        project_names.append(name)



    def submit(self):
        self.browser.find_element(By.CSS_SELECTOR, 'input[value="Zapisz"]').click()

    def wait_for_succes(self):
        wait = WebDriverWait(self.browser, 10)
        selector = (By.CSS_SELECTOR, '.j_close_button')
        return wait.until(expected_conditions.element_to_be_clickable(selector))


